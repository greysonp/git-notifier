var app = require('express')();
var bodyParser = require('body-parser');
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(bodyParser.json());

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

app.get('/hook', translateGithubToHipchat);
app.post('/hook', translateGithubToHipchat);

io.on('connection', function(socket){
    console.log('a user connected');

    socket.on('disconnect', function(){
        console.log('user disconnected');
    });
});

function translateGithubToHipchat(req, res) {
    console.log('Got request.');
    var notification = getMessageForEvent(req.get('X-Github-Event'), req.body);
    console.log(notification);
    io.emit('notification', notification);
    res.send('');
}

function getMessageForEvent(event, data) {
    var out = {
        'title': '',
        'message': '',
        'html': '',
        'url': null,
        'image': null
    }

    if (event == 'pull_request') {
        out.title = 'Pull request ' + data.action;
        out.message = 'Pull request ' + data.action + ' by ' + data.pull_request.user.login;
        out.html += 'Pull request ' + data.action + ' by ' + data.pull_request.user.login;
        out.url = data.pull_request.html_url;
        out.image = data.pull_request.user.avatar_url;
    } else if (event == 'pull_request_review_comment') {
        out.title = data.pull_request.title;
        out.message = 'New comment by ' + data.comment.user.login;
        out.html += '<strong>' + data.pull_request.title + '</strong><br />';
        out.html += 'Comment ' + data.action + ' by ' + data.comment.user.login + '<br />';
        out.html += '<pre>' + data.comment.diff_hunk + '</pre><br />';
        out.html += '<blockquote>' + data.comment.body + '</blockquote>';
        out.url = data.comment.url;
        out.image = data.comment.user.avatar_url;
    } else if (event == 'push') {
        out.title = 'New push';
        out.html = data.pusher.name + ' pushed some commits.';
        out.image = data.sender.avatar_url;
    } else if (event == 'issue_comment') {
        out.title = data.pull_request.title;
        out.message = 'New comment by ' + data.sender.login;
        out.html += '<strong>' + data.pull_request.title + '</strong><br />';
        out.html += 'Comment ' + data.action + ' by ' + data.sender.login + '<br />';
        out.html += '<blockquote>' + data.pull_request.body + '</blockquote>';
        out.image = data.sender.avatar_url;
    } else {
        out.title = 'Event: ' + event;
        out.html = 'Event: ' + event;
    }

    return out;
}

var server = http.listen((process.env.PORT || 3000), function(){
    console.log('listening on *:' + server.address().port);
});